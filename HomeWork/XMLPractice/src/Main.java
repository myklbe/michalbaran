import org.w3c.dom.Document;
import org.xml.sax.SAXException;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Created by Lenovo on 17.06.2017.
 */
public class Main {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

        MethodsXML method = new MethodsXML();
//        try {
//            System.out.println(method.getNames());
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SAXException e) {
//            e.printStackTrace();
//        } catch (ParserConfigurationException e) {
//            e.printStackTrace();
//        }
        Document doc = method.createStudent("Jan", "Kowal", "4");
        method.addStudent(doc,"Krzysztof", "Larki", "3");
        try {
            method.saveFile(doc, "students.xml");
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        ;


    }


}
