/**
 * Created by Lenovo on 14.06.2017.
 */
public class User {
    private String name;
    private String seconndName;
    private Address address;
    private String phone;
    private UserSex sex;
    private String CCN;
    private String PESEL;
    private Date birthDate;

    public User(String name, String seconndName, Address address, String phone, UserSex sex, String CCN, String PESEL, Date birthDate) {
        this.name = name;
        this.seconndName = seconndName;
        this.address = address;
        this.phone = phone;
        this.sex = sex;
        this.CCN = CCN;
        this.PESEL = PESEL;
        this.birthDate = birthDate;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeconndName() {
        return seconndName;
    }

    public void setSeconndName(String seconndName) {
        this.seconndName = seconndName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserSex getSex() {
        return sex;
    }

    public void setSex(UserSex sex) {
        this.sex = sex;
    }

    public String getCCN() {
        return CCN;
    }

    public void setCCN(String CCN) {
        this.CCN = CCN;
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "User: " +
                name + " " +
                seconndName + "\n" +
                address + "\n" +
                "phone : " + phone + "\n" +
                sex.getName() + "\n" +
                "Credit Card: " + CCN + "\n" +
                PESEL + "\n" +
                birthDate;
    }

}
